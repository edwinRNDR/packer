package net.lustlab.packer;

/**
 * Visitor interface to visit PackNode instances
 * @author Edwin Jakobs
 *
 */
public interface PackNodeVisitor {
	
	/**
	 * Called when visiting a node.
	 * @param node The node currently visited
	 */
	public abstract void visit(PackNode node);
}
