package net.lustlab.packer;


public class CenteredBinarySplitter implements Splitter {

	private final Rectangle enclosement;
	boolean invert = false;
	
	public CenteredBinarySplitter(Rectangle enclosement) {
		this.enclosement = enclosement;
	}
	
	public void setInvert(boolean invert) {
		this.invert = invert;
	}
	
	public PackNode[] split(PackNode node, Rectangle rect) {

		// dimension deltas
		float dw = node.getArea().width - rect.width;
		float dh = node.getArea().height - rect.height;

		float s = invert? 1 : -1;

		float sanding = 0; //getSanding();

		Rectangle lrect, rrect;
		if (dw > dh) {

			if (s*(node.getArea().topLeft.x - enclosement.topLeft.x) > s*enclosement.width/2 ) {
				lrect = new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y, rect.width, node.getArea().height);
				rrect = new Rectangle(node.getArea().topLeft.x+rect.width + sanding, node.getArea().topLeft.y, node.getArea().width-rect.width-sanding, node.getArea().height);
			}
			else {
				float ew = node.getArea().width - rect.width - sanding;
				rrect = new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y, ew, node.getArea().height);
				lrect = new Rectangle(node.getArea().topLeft.x+ew+sanding, node.getArea().topLeft.y, rect.width, node.getArea().height);	
			}
		}
		else {

			if (s*(node.getArea().topLeft.y - enclosement.topLeft.y) > s*enclosement.height /2) {
				lrect = new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y, node.getArea().width, rect.height);
				rrect =  new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y + rect.height + sanding , node.getArea().width, node.getArea().height-rect.height-sanding);
			}
			else {
				float eh = node.getArea().height - rect.height - sanding;
				rrect = new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y, node.getArea().width, eh);
				lrect =  new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y + eh+sanding, node.getArea().width, rect.height);
			}
		}

		return new PackNode[] {new PackNode(lrect), new PackNode(rrect) };
	}

}
