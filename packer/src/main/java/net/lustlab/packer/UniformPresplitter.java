package net.lustlab.packer;


public class UniformPresplitter implements Presplitter {

	int verticalDivisions;
	int horizontalDivisions;
	
	public UniformPresplitter(int verticalDivisions, int horizontalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
		this.verticalDivisions = verticalDivisions;
	}
	
	public void setHorizontalDivisions(int horizontalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
	}
	public void setVerticalDivisions(int verticalDivisions) {
		this.verticalDivisions = verticalDivisions;
	}
	public int getHorizontalDivisions() {
		return horizontalDivisions;
	}
	public int getVerticalDivisions() {
		return verticalDivisions;
	}
	
	
	public PackNode[] split(PackNode input) {
		
		float splitWidth = input.getArea().width / horizontalDivisions;
		float splitHeight = input.getArea().height / verticalDivisions;

		PackNode[] result = new PackNode[verticalDivisions * horizontalDivisions];
		
		Vector topLeft = input.getArea().topLeft;
		
		for (int j = 0; j < verticalDivisions; ++j) {
			
			for (int i = 0; i < horizontalDivisions;++i) {
				Rectangle rect = new Rectangle(topLeft.x + i * splitWidth, topLeft.y + j * splitHeight, splitWidth, splitHeight);
				result[j * horizontalDivisions + i] = new PackNode(rect);
			}
		}
		
		return result;
	}

}
