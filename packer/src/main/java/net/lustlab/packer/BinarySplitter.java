package net.lustlab.packer;


public class BinarySplitter implements Splitter {

	public PackNode[] split(PackNode node, Rectangle rect) {
		float dw = node.getArea().width - rect.width;
		float dh = node.getArea().height - rect.height;

		float sanding = 0; //getSanding();
		
		Rectangle lrect, rrect;
		if (dw > dh) {
			lrect = new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y, rect.width, node.getArea().height);
			rrect = new Rectangle(node.getArea().topLeft.x+rect.width+sanding, node.getArea().topLeft.y, node.getArea().width-rect.width-sanding, node.getArea().height);
		}
		else {
			lrect = new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y, node.getArea().width, rect.height);
			rrect = new Rectangle(node.getArea().topLeft.x, node.getArea().topLeft.y + rect.height + sanding, node.getArea().width, node.getArea().height-rect.height-sanding);
		}
		
		return new PackNode[] {new PackNode(lrect), new PackNode(rrect) };	}

}
