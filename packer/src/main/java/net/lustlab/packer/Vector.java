package net.lustlab.packer;

import java.io.Serializable;

/**
 * Two dimensional mathematical vector class
 * @author Edwin Jakobs
 *
 */
public class Vector implements Serializable {
	/** The x coordinate.*/
	public float x;
	/** The y coordinate.*/
	public float y;
	
	/**
	 * Vector constructor with x and y arguments.
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public Vector(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Default vector constructor.
	 */
	Vector() {
		this(0,0);
	}

}
