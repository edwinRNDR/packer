package net.lustlab.packer;


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Tools for {@code PackNode}.
 * @author Edwin Jakobs
 *
 */
public class PackNodeTools {


	/**
	 * Visit all nodes and applies visitor.
	 * @param root the root node
	 * @param visitor the {@code PackNodeVisitor} instance to apply to each visited node
	 */
	public static void visitNodes(PackNode root, PackNodeVisitor visitor) {
		Stack<PackNode> stack = new Stack<PackNode>();
		stack.push(root);
		while (!stack.empty()) {
			PackNode node = stack.pop();
			visitor.visit(node);

			if (!node.isLeaf()) {
				for (PackNode c: node.children)
					stack.push(c);
			}
		}		
	}

	/**
	 * Collect all leaf nodes starting from the root node.
	 * @param root the root node from which to start searching for leaf nodes
	 * @return a list containing all leaf nodes
	 */
	public static List<PackNode> findLeafNodes(PackNode root) {

		Stack<PackNode> stack = new Stack<PackNode>();
		ArrayList<PackNode> leafNodes = new ArrayList<PackNode>();

		stack.push(root);
		while (!stack.empty()) {
			PackNode node = stack.pop();

			if (node.isLeaf()) {
				leafNodes.add(node);

			}
			else {
				for (PackNode c: node.children)
					stack.push(c);
			}
		}
		return leafNodes;
	}
}
