package net.lustlab.packer;



public class GutterPresplitter implements Presplitter {

	int verticalDivisions;
	int horizontalDivisions;
	float gutter;
	
	public void setGutter(float gutter) {
		this.gutter = gutter;
	}
	
	public GutterPresplitter(int verticalDivisions, int horizontalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
		this.verticalDivisions = verticalDivisions;
	}
	
	public void setHorizontalDivisions(int horizontalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
	}
	public void setVerticalDivisions(int verticalDivisions) {
		this.verticalDivisions = verticalDivisions;
	}
	public int getHorizontalDivisions() {
		return horizontalDivisions;
	}
	public int getVerticalDivisions() {
		return verticalDivisions;
	}
	
	
	public PackNode[] split(PackNode input) {
		
		
		float horizontalGutter = horizontalDivisions > 1 ? gutter : 0;
		float verticalGutter = verticalDivisions > 1 ? gutter : 0;
		
		float splitWidth = input.getArea().width / horizontalDivisions - horizontalGutter;
		float splitHeight = input.getArea().height / verticalDivisions - verticalGutter;

		PackNode[] result = new PackNode[(verticalDivisions*2-1) * (horizontalDivisions*2-1)];
		
		float[] xs = new float[horizontalDivisions * 2 -1];
		float[] ys = new float[verticalDivisions * 2 -1];

		{	int idx = 0;
			float x = input.getArea().topLeft.x;

			for (int i = 0; i < horizontalDivisions;++i) {
				xs[idx++] = x;
				x+=splitWidth;
				if (idx < xs.length-1) {
					xs[idx++] = x;
					x+=gutter;
				}
			}
		}

		{	int idx = 0;
			float y = input.getArea().topLeft.y;
			for (int i = 0; i < verticalDivisions;++i) {
				ys[idx++] = y;
				y+=splitHeight;
				if (idx < ys.length-1) {
					ys[idx++] = y;
					y+=gutter;
				}
			}
		}

		
		for (int j = 0; j < verticalDivisions*2-1; ++j) {

			for (int i = 0; i < horizontalDivisions*2-1;++i) {
				Rectangle rect = new Rectangle(	xs[i],
						ys[j], 
						(i%2)==0 ? splitWidth : gutter, 
						(j%2)==0 ? splitHeight: gutter);
				result[j * (horizontalDivisions*2-1) + i] = new PackNode(rect);
			}
		}
		
		return result;
	}

}
