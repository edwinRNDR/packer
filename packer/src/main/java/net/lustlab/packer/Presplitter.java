package net.lustlab.packer;


public interface Presplitter {

	public PackNode[] split(PackNode input);
	
}
