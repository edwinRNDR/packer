package net.lustlab.packer;

import java.io.Serializable;

/**
 * Rectangle class.
 * @author Edwin Jakobs
 *
 */

public class Rectangle implements Serializable {
	
	/** The top-left corner of the rectangle.*/
	public Vector topLeft;
	/** The width of the rectangle.*/
	public float width;
	/** The height of the rectangle. */
	public float height;
	
	public long birth = System.currentTimeMillis();
	
	
	public long age() {
		return System.currentTimeMillis() - birth;
	}
	
	
	/**
	 * Rectangle constructor.
	 * @param topLeft the top left of the rectangle
	 * @param width the width of the rectangle
	 * @param height the height of the rectangle
	 */
	public Rectangle(Vector topLeft, float width, float height) {
		this.topLeft = new Vector(topLeft.x, topLeft.y);
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Rectangle constructor with scalar arguments.
	 * @param x the left most x coordinate of the rectangle
	 * @param y the topmost y coordinate of the rectangle
	 * @param width the width of the rectangle
	 * @param height the height of the rectangle
	 */
	public Rectangle(float x, float y, float width, float height) {
		topLeft = new Vector(x,y);
		this.width = width;
		this.height = height;
	}
}
