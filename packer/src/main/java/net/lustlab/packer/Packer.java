package net.lustlab.packer;


/**
 * Packer base class.
 * @author Edwin Jakobs
 *
 */
public class Packer {

	private float sanding = 0;

	private Clipper clipper = null;
	private Orderer orderer = null;
	private Splitter splitter = null;
	
	
	public Packer(Orderer orderer, Splitter splitter, Clipper clipper) {
		this.orderer = orderer;
		this.splitter = splitter;
		this.clipper = clipper;
	}
	public void setClipper(Clipper clipper) {
		this.clipper = clipper;
	}
	
	public void setOrderer(Orderer orderer) {
		this.orderer = orderer;
	}
	public void setSplitter(Splitter splitter) {
		this.splitter = splitter;
	}
	
	
	/**
	 * Sets the sanding to be used between cells.
	 * @param sanding the (unitless) amount of sanding
	 */
	public void setSanding(float sanding) {
		this.sanding = sanding;
	}
	
	/**
	 * Gets the sanding to be used between cells.
	 * @return the (unitless) amount of sanding to be used between cells 
	 */
	 public float getSanding() {
		return sanding;
	}
	

	
	/**
	 * Inserts a {@code Rectangle} into the {@code PackNode} tree.
	 * @param node the root node at which to start inserting
	 * @param rect the rectangle to insert
	 * @return the PackNode in which the rectangle is inserted or null when insertion failed 
	 */
	public PackNode insert(PackNode node, Rectangle rect) {
		return insert(node, rect, null);
	}
	
	/**
	 * Inserts a {@code Rectangle} into the {@code PackNode} tree with user data.
	 * @param node the root node at which to start inserting
	 * @param rect the rectangle to insert
	 * @param data user data that will be stored in the {@code PackNode}
	 * @return the PackNode in which the rectangle is inserted or null when insertion failed 
	 */
	public PackNode insert(PackNode node, Rectangle rect, Object data) {

		if (!node.isLeaf()) {

			if (!(rect.width <= node.getArea().width && rect.height <= node.getArea().height)) {
				return null;
			}	

			PackNode newNode;

			int[] order = orderer.order(node, rect);

			for (int i: order) {

				newNode = insert(node.children[i], rect, data); //children[i].insert(rect, id);
				if (newNode != null)
					return newNode;
			}
			return null;

		}
		else {
			if (node.isTaken())
				return null;
			else {
				if (node.getArea().width < rect.width || node.getArea().height < rect.height) {
					// area too small. cannot fit
					return null;
				}

				else if (node.getArea().width == rect.width && node.getArea().height == rect.height) {
					// perfect fit!
					if (clipper == null || clipper.inside(node.getArea(),rect)) {
					
					node.populate(data);
						return node;
					}
					else {
						return null;
					}
				}
				else {
					// fits, but area too big => split area
					node.children = splitter.split(node, rect);
					return insert(node.children[0], rect, data);

				}
			}
		}
	}




}
