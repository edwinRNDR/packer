package net.lustlab.packer;


public interface Splitter {
	public PackNode[] split(PackNode node, Rectangle rect);

}
