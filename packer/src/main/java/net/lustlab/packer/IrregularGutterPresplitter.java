package net.lustlab.packer;



public class IrregularGutterPresplitter implements Presplitter {

	float[] verticalDivisions;
	float[] horizontalDivisions;
	float gutter;
	
	boolean outputGutters = true;
	boolean outputCells = true;
	
	public void setOutputCells(boolean outputCells) {
		this.outputCells = outputCells;
	}
	public void setOutputGutters(boolean outputGutters) {
		this.outputGutters = outputGutters;
	}
	public boolean isOutputCells() {
		return outputCells;
	}public boolean isOutputGutters() {
		return outputGutters;
	}
	
	public void setGutter(float gutter) {
		this.gutter = gutter;
	}
	
	public IrregularGutterPresplitter(float[] verticalDivisions, float[] horizontalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
		this.verticalDivisions = verticalDivisions;
	}
	
	public void setHorizontalDivisions(float[] horizontalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
	}
	public void setVerticalDivisions(float[] verticalDivisions) {
		this.verticalDivisions = verticalDivisions;
	}
	public float[] getHorizontalDivisions() {
		return horizontalDivisions;
	}
	public float[] getVerticalDivisions() {
		return verticalDivisions;
	}
	
	
	public PackNode[] split(PackNode input) {
				
		int hl = horizontalDivisions.length;
		float hg = hl > 1 ? gutter : 0;
		int vl = verticalDivisions.length;
		float vg = vl > 1 ? gutter : 0;

		float[] xs = new float[hl * 2 - 1];
		float[] ys = new float[vl * 2 - 1];
		float widths[] = new float[hl * 2 - 1];
		float heights[] = new float[vl * 2 - 1];
		
		{	int idx = 0;
			float x = input.getArea().topLeft.x;

			for (int i = 0; i < hl;++i) {
				xs[idx] = x;
				x+=widths[idx] = (input.getArea().width-hg*(hl-1)) * horizontalDivisions[i];
				idx++;
				if (idx < xs.length-1) {
					xs[idx] = x;
					x+=widths[idx] = gutter;
					idx++;
				}
			}
		}

		{	int idx = 0;
			float y = input.getArea().topLeft.y;
			for (int i = 0; i < vl;++i) {
				ys[idx] = y;
				y+=heights[idx] = (input.getArea().height-vg*(hl-1)) * verticalDivisions[i];
				idx++;
				if (idx < ys.length-1) {
					ys[idx] = y;
					y+=heights[idx] = gutter;
					idx++;
				}
			}
		}
		
		int cellCount = vl * hl;
		int gutterCount = 3*vl*hl - 2*vl - 2*hl + 1;
		
		PackNode[] result = new PackNode[(outputCells? cellCount : 0) + (outputGutters? gutterCount : 0)];

		int idx = 0;
		for (int j = 0; j < vl*2-1; ++j) {
			for (int i = 0; i < hl*2-1; ++i) {
				boolean isCell = (j%2==0) && (i%2==0);
				if ((outputCells && isCell) || (outputGutters && !isCell)) {
					result[idx++] = new PackNode(new Rectangle(xs[i],ys[j],widths[i],heights[j]));
				}
			}
		}
		
		
		return result;
	}

}
