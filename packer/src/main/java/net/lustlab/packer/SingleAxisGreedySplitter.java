package net.lustlab.packer;


public class SingleAxisGreedySplitter implements Splitter {
	private float bias = 0;
	/**
	 * Returns the insertion bias
	 * @param bias the bias [-1, 1]
	 */
	
	int divisions = 1;
	
	public void setDivisions(int divisions) {
		this.divisions = divisions;
	}
	
	public void setBias(float bias) {
		this.bias = bias;
	}

	/**
	 * Gets the insertion bias
	 * @return the bias [-1, 1]
	 */
	public float getBias() {
		return bias;
	}

	public PackNode[] split(PackNode node, Rectangle rect) {




		int dc = divisions * 2 + 1;
		
		float b = (bias + 1)/2;
		
		float leftMargin = b * (node.getArea().width - rect.width ) / divisions ;
		float topMargin = b * (node.getArea().height - rect.height) / divisions ;
		float rightMargin = (1-b) * (node.getArea().width - rect.width ) / divisions;
		float bottomMargin = (1-b) * (node.getArea().height - rect.height)  / divisions ;


		float x0 = node.getArea().topLeft.x;
		float y0 = node.getArea().topLeft.y;

		PackNode[] results = new PackNode[dc];
		Rectangle[] rects = new Rectangle[dc];

		float dw = node.getArea().width - rect.width;
		float dh = node.getArea().height - rect.height;


		float widths[] = new float[dc];
		float heights[] = new float[dc];

		
		float xpos[] = new float[dc]; //{ x0, x0+leftMargin, x0+leftMargin+rect.width };
		float ypos[] = new float[dc]; // { y0, y0+topMargin, y0 + topMargin + rect.height + sanding };

		float x = node.getArea().topLeft.x;
		float y = node.getArea().topLeft.y;

		
		for (int i = 0; i < dc; ++i) {
			xpos[i] = x;		
			ypos[i] = y;			
			
			if (i < dc/2) {
				x += leftMargin;
				widths[i] = leftMargin;
				y += topMargin;
				heights[i] = topMargin;
			}
			else if (i == dc/2) {
				x+= rect.width;
				widths[i] = rect.width;
				y+= rect.height;
				heights[i] = rect.height;
			}
			else {
				x+= rightMargin;
				widths[i] = rightMargin;
				y+= bottomMargin;
				heights[i] = bottomMargin;

			}
		}

		
		
		if (dw > dh) {
			for (int i = 0; i < dc; ++i)
				rects[i] = new Rectangle(xpos[i], y0, widths[i], node.getArea().height);
		}
		else {
			for (int i = 0; i < dc; ++i)
				rects[i] = new Rectangle(x0, ypos[i], node.getArea().width, heights[i]);
		}


		int count = 0;
		int[] order = new int[dc];
		order[0] = (dc/2);
		
		for (int i = 1; i < order.length; ++i) {
			order[i] = count;
			count++;
			if  (count == order[0]) {
				count++;
			}
		}
		for (int i = 0; i < dc; ++i) {
			results[i] = new PackNode(rects[order[i]]);
		}
		
		return results;
	}

}
