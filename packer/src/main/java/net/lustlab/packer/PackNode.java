package net.lustlab.packer;

import java.io.Serializable;

/**
 * PackNode represents a single node in a packing tree.
 * @author Edwin Jakobs
 */
public class PackNode implements Serializable {
	private Rectangle area;
	public PackNode[] children = null;
	private boolean taken = false;
	private Object data;
	
	
	public void setData(Object data) {
		this.data = data;
	}
	public boolean isTaken() {
		return taken;
	}
	
	/**
	 * PackNode constructor.
	 * @param rect the area of the PackNode
	 */
	public PackNode(Rectangle rect) {
		area = rect;
	}

	/**
	 * Returns the area of the PackNode.
	 * @return a Rectangle representing the area of the PackNode
	 */
	public Rectangle getArea() {
		return area;
	}
	
	/**
	 * Determines if this node is a leaf node.
	 * @return true if this node has no child nodes
	 */
	public boolean isLeaf() {
		
		if (children == null)
			return true;
		
		for (PackNode c: children) {
			if (c!=null)
				return false;
		}
		return true;
	}
	
	public void evacuate() {
		assert(this.taken);
		this.taken = false;
		this.data = null;
	}
	
	
	public void populate(Object data) {
		
		assert(!this.taken);
		
		this.taken = true;
		this.data = data;
	}
	
	public Object getData() {
		return data;
	}
}


