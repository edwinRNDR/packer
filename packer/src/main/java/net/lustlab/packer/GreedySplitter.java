package net.lustlab.packer;


public class GreedySplitter implements Splitter {
	private float horizontalBias = 0;
	private float verticalBias = 0;
	
	int horizontalDivisions = 1;
	int verticalDivisions = 1;
	
	public void setHorizontalDivisions(int horizontalDivisions) {
		this.horizontalDivisions = horizontalDivisions;
	}
	
	public void setVerticalDivisions(int verticalDivisions) {
		this.verticalDivisions = verticalDivisions;
	}
	public int getHorizontalDivisions() {
		return horizontalDivisions;
	}
	
	public int getVerticalDivisions() {
		return verticalDivisions;
	}
	

	/**
	 * Sets the vertical bias. How much the inserted rectangle should be offset. 
	 * @param verticalBias the vertical bias [-1, 1]
	 */
	public void setVerticalBias(float verticalBias) {
		this.verticalBias = verticalBias;
	}
	
	/**
	 * 
	 * @param horizontalBias the horizontal bias [-1, 1]
	 */
	public void setHorizontalBias(float horizontalBias) {
		this.horizontalBias = horizontalBias;
	}
	
	public float getHorizontalBias() {
		return horizontalBias;
	}
	
	public float getVerticalBias() {
		return verticalBias;
	}
	
	
	public PackNode[] split(PackNode node, Rectangle rect) {
		
		int hc = horizontalDivisions * 2 + 1;
		int vc = verticalDivisions * 2 + 1;
		
		float hb = (horizontalBias + 1)/2;
		float vb = (verticalBias + 1)/2;
		
		float leftMargin = hb * (node.getArea().width - rect.width ) / horizontalDivisions ;
		float topMargin = vb * (node.getArea().height - rect.height) / verticalDivisions ;
		float rightMargin = (1-hb) * (node.getArea().width - rect.width ) / horizontalDivisions;
		float bottomMargin = (1-vb) * (node.getArea().height - rect.height)  / verticalDivisions ;

		PackNode[] result = new PackNode[hc * vc];
		Rectangle[] rects = new Rectangle[hc * vc];

		float x = node.getArea().topLeft.x;
		float y = node.getArea().topLeft.y;
		
		float xpos[] = new float[hc]; //{ x0, x0+leftMargin, x0+leftMargin+rect.width };
		float ypos[] = new float[vc]; // { y0, y0+topMargin, y0 + topMargin + rect.height + sanding };
		float widths[] = new float[hc];
		float heights[] = new float[vc];
		for (int i = 0; i < hc; ++i) {
			xpos[i] = x;		
			
			if (i < hc/2) {
				x += leftMargin;
				widths[i] = leftMargin;
			}
			else if (i == hc/2) {
				x+= rect.width;
				widths[i] = rect.width;

			}
			else {
				x+= rightMargin;
				widths[i] = rightMargin;
			}
		}

		//
		
		for (int i = 0; i < vc; ++i) {
			ypos[i] = y;			
			if (i < vc/2) {
				y += topMargin;
				heights[i] = topMargin;
			}
			else if (i == vc/2) {
				y+= rect.height;
				heights[i] = rect.height;
			}
			else {
				y+= bottomMargin;
				heights[i] = bottomMargin;
			}
		}

		
		for (int j = 0; j < vc; ++j) {
			for (int i = 0; i < hc; ++i) {
				rects[j*hc+i] = new Rectangle(xpos[i], ypos[j], widths[i], heights[j]);
			}
		}

		int[] order = new int[hc*vc];
		order[0] = (vc/2) * hc + (hc/2);
		int count = 0;
		for (int i = 1; i < order.length; ++i) {
			order[i] = count;
			count++;
			if  (count == order[0]) {
				count++;
			}
		}
		
		for (int i = 0; i < hc * vc; ++i) {
			result[i] = new PackNode(rects[order[i]]);
		}

		return result;	
	}
	
}
