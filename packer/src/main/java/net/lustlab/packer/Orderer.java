package net.lustlab.packer;


public interface Orderer {
	public int[] order(PackNode node, Rectangle rect);
}
