package net.lustlab.packer;

import java.util.ArrayList;
import java.util.Collections;


public class RandomOrderer implements Orderer {

	public int[] order(PackNode node, Rectangle rect) {
		int[] order = new int[node.children.length];
		for (int i = 0; i <  order.length; ++i) {
			order[i] = i;
		}
		
		ArrayList<Integer> orderList = new ArrayList<Integer>();

		for (int i = 0; i < node.children.length; ++i) {
			orderList.add(new Integer(i));
		}

		Collections.shuffle(orderList);

		for (int i = 0; i < orderList.size(); ++i) {
			order[i] = orderList.get(i);
		}

		return order;
	}

}
