package net.lustlab.packer;


/**
 * Clipper interface
 * @author Edwin Jakobs (edwin@lustlab.net)
 */
public interface Clipper {

	/**
	 * Determines if a rectangle should be inserted 
	 * @param area the area of the candidate node in which the rectangle will be inserted
	 * @param rectangle the 
	 * @return true if rectangle should be inserted into area
	 */
	boolean inside(Rectangle area, Rectangle rectangle);
}
