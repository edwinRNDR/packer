package net.lustlab.packer;


public class AsIsOrderer implements Orderer {
	public int[] order(PackNode node, Rectangle rect) {
		int[] order = new int[node.children.length];
		
		for (int i = 0; i < order.length; ++i) {
			order[i] = i;
		}
		
		return order;
	}
}
